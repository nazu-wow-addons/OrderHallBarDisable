local f = CreateFrame("Frame")
f:SetScript("OnUpdate", function(self,...)
    if OrderHallCommandBar then
        OrderHallCommandBar:Hide()
        OrderHallCommandBar:UnregisterAllEvents()
        OrderHallCommandBar.Show = function() end
    end
    OrderHall_CheckCommandBar = function () end
    self:SetScript("OnUpdate", nil)
end)
