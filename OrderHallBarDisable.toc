## Title: OrderHallBarDisable
## Notes: "Disable the Class Order Hall bar"
## Author: nazuraki
## Version: 1.0
## Interface: 70000
## X-Website: https://gitlab.com/nazu-wow-addons/OrderHallBarDisable
## LoadOnDemand: 1
## Dependencies: Blizzard_OrderHallUI
## LoadWith: Blizzard_OrderHallUI

OrderHallBarDisable.lua
